Honeywords
==========

Description
-----------
WARNING, this module is not yet finished! Do not use it!

Honeywords is a module that will generate fake passwords ("honeywords") for
every user on the site. The main use of this module is to make it harder for
an attacker to penetrate your site without being detected (and notifying you
of data leaks that may otherwise have gone unnoticed). It has the side benefit
of making the password database generally less useful for hackers targetting
your users.

Honeywords is an attempt at implementing the Juels-Rivest MIT paper:
http://people.csail.mit.edu/rivest/honeywords/

To be clear, of the many attack scenarios listed in the paper, this module
(and this concept) only addresses a "Stolen [list] of password hashes" scenario.
There are other modules to help you implement security policies addressing the
others. Preventing data leaks in the first place is of course more important,
but damage control should be a part of any responsible security policy.

Limitations
----------
A lot of things can diminish or nullify the usefulness of this module:
 * Using the "local file"/no server option is not a good idea.
 * If permissions aren't set correctly, and you're using the "local file"/none
   server option, an intruder may gain read access to the key file.
 * If an attacker can execute arbitrarycode on your server/site, you are prett
   much screwed no matter what other security measures you many be using.
 * If you don't keep your core and contrib modules up to date, they may contain
   security flaws that could allow attackers to execute arbitrary code on the
   site. See previous point.
 * If all else is perfect, the users' passwords are only as safe as the
   honeychecker, and of course the honeychecker/server connection.

Credits
-------
This is a project I (gboudrias) am doing in my spare time, for the love
of free software and security. Don't hesitate to contact me if you can help,
or open issues if you see missing features or implementation errors.

Of course this is just an implementation, the real work is in the previously
linked Juels-Rivest paper.
